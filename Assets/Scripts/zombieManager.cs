using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class zombieManager : MonoBehaviour
{
    // Start is called before the first frame update
   // Start is called before the first frame update
    private Rigidbody2D r2d;
    private Animator animator;
    private SpriteRenderer sr;
    public float corr=5;
    void Start()
    {
         sr = GetComponent<SpriteRenderer>();
         animator = GetComponent<Animator>();
         r2d = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        sr.flipX = true;
        r2d.velocity = new Vector2(-corr, r2d.velocity.y);
    }
}
    
