
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    // Start is called before the first frame update
    private bool Saltar = false;
     private SpriteRenderer sr;
    private Animator animator;
    private Rigidbody2D rb2d;
    private int salto = 0;
   public float correr=10;
   public bool dead=false;
    void Start()
    {
        sr = GetComponent<SpriteRenderer>(); 
        animator = GetComponent<Animator>();
        rb2d = GetComponent<Rigidbody2D>();
        
    }

    // Update is called once per frame
    void Update()
    {
         SetIdleAnimation();
         if(Input.GetKeyDown(KeyCode.Space) && Saltar && salto < 1)
        {
            var jump = 20f;
            SetJumpAnimation();
            rb2d.velocity = Vector2.up * jump;
            salto++;
        }else{

             rb2d.velocity = new Vector2(correr, rb2d.velocity.y);
             SetRunAnimation();
        }if(dead==true){
                SetDeadAnimation();
                 rb2d.velocity = new Vector2(0, rb2d.velocity.y);
                  Saltar = false;

            }
    }
     void OnCollisionEnter2D(Collision2D other)
    {
        Saltar = true;
        salto = 0;
        if(other.gameObject.layer == 6){
            dead=true;
            Debug.Log("other.gameObject.name");
        }
        
        
        

        
    }
    private void SetIdleAnimation()
    {
        animator.SetInteger("Estado",0);

    }
    private void SetJumpAnimation()
    {
        animator.SetInteger("Estado",1);

    }
     private void SetRunAnimation()
    {
        animator.SetInteger("Estado",2);

    }
     private void SetDeadAnimation()
    {
        animator.SetInteger("Estado",3);

    }
}
